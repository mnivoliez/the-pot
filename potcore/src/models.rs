use diesel;
use diesel::prelude::*;

use schema::*;

#[derive(Queryable, Identifiable)]
#[table_name = "ingredients"]
pub struct Ingredient {
    pub id: i32,
    pub name: String,
}

#[derive(Insertable)]
#[table_name = "ingredients"]
pub struct NewIngredient<'a> {
    pub name: &'a str,
}

#[derive(Queryable, Identifiable)]
#[table_name = "recipes"]
pub struct Recipe {
    pub id: i32,
    pub title: String,
    pub description: String,
    pub duration: diesel::sql_types::Interval,
}

#[derive(Insertable)]
#[table_name = "recipes"]
pub struct NewRecipe {
    pub title: String,
    pub description: String,
    pub duration: diesel::sql_types::Interval,
}

#[derive(Queryable, Associations, Identifiable)]
#[belongs_to(Recipe)]
#[table_name = "steps"]
pub struct Step {
    pub id: i32,
    pub instruction: String,
    pub recipe_id: i32,
    pub duration: Option<diesel::sql_types::Interval>,
}

#[derive(Insertable)]
#[table_name = "steps"]
pub struct NewStep {
    pub instruction: String,
    pub recipe_id: i32,
    pub duration: Option<diesel::sql_types::Interval>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy)]
pub enum QuantityUnit {
    g,
    kg,
    l,
    dl,
    cl,
    ml,
    tsp,
    tbsp,
}

#[derive(Queryable, Associations, Identifiable)]
#[primary_key(recipe_id, ingredient_id)]
#[belongs_to(Recipe)]
#[belongs_to(Ingredient)]
#[table_name = "recipes_ingredients"]
pub struct RecipeIngredient {
    pub recipe_id: i32,
    pub ingredient_id: i32,
    pub quatity: f32,
    pub unit: QuantityUnit,
}

#[derive(Insertable)]
#[table_name = "recipes_ingredients"]
pub struct NewRecipeIngredient {
    pub recipe_id: i32,
    pub ingredient_id: i32,
    pub quatity: f32,
    pub unit: QuantityUnit,

}
