#[macro_use]
extern crate diesel;

use diesel::pg::PgConnection;
use diesel::prelude::*;

pub mod models;
pub mod schema;

pub fn establish_connection(database_url: &str) -> PgConnection {
    PgConnection::establish(database_url).expect(&format!("Error connecting to {}", database_url))
}

use self::models::{Ingredient, NewIngredient};

pub fn create_ingredient<'a>(conn: &PgConnection, name: &'a str) -> Ingredient {
    use schema::ingredients;

    let new_ingredient = NewIngredient { name: name };

    diesel::insert_into(ingredients::table)
        .values(&new_ingredient)
        .get_result(conn)
        .expect("Error saving new post")
}
