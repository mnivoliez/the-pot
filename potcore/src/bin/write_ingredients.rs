extern crate diesel;
extern crate potcore;

use self::potcore::*;
use std::io::{stdin, Read};

use std::env;

fn main() {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let connection = establish_connection(&database_url);

    println!("What would you like your ingredient to be?");
    let mut name = String::new();
    stdin().read_line(&mut name).unwrap();
    let name = &name[..(name.len() - 1)]; // Drop the newline character

    let ingredient = create_ingredient(&connection, name);
    println!("\nSaved ingredient {} with id {}", name, ingredient.id);
}
