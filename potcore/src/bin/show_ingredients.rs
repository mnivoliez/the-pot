extern crate diesel;
extern crate potcore;

use std::env;

use self::models::*;
use self::potcore::*;
use diesel::prelude::*;

fn main() {
    use self::schema::ingredients::dsl::*;

    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let connection = establish_connection(&database_url);

    let results = ingredients
        .load::<Ingredient>(&connection)
        .expect("Error loading ingredients.");

    println!("Displaying {} ingredients", results.len());
    for ingredient in results {
        println!("{}: {}", ingredient.id, ingredient.name);
    }
}
