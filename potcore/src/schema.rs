table! {
    ingredients (id) {
        id -> Int4,
        name -> Varchar,
    }
}

table! {
    recipes (id) {
        id -> Int4,
        title -> Varchar,
        description -> Nullable<Text>,
        duration -> Nullable<Interval>,
    }
}

table! {
    recipes_ingredients (recipe_id, ingredient_id) {
        recipe_id -> Int4,
        ingredient_id -> Int4,
        quantity -> Float8,
        unit -> Varchar,
    }
}

table! {
    steps (id) {
        id -> Int4,
        instruction -> Text,
        recipe_id -> Nullable<Int4>,
        duration -> Nullable<Interval>,
    }
}

joinable!(recipes_ingredients -> ingredients (ingredient_id));
joinable!(recipes_ingredients -> recipes (recipe_id));
joinable!(steps -> recipes (recipe_id));

allow_tables_to_appear_in_same_query!(ingredients, recipes, recipes_ingredients, steps,);
