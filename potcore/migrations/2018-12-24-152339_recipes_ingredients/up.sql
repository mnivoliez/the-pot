-- Your SQL goes here
CREATE TABLE recipes_ingredients (
  recipe_id INTEGER REFERENCES recipes(id),
  ingredient_id INTEGER REFERENCES ingredients(id),
  quantity FLOAT NOT NULL,
  unit VARCHAR NOT NULL DEFAULT 'g',
  PRIMARY KEY(recipe_id, ingredient_id)
);
