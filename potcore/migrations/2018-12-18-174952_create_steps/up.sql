-- Your SQL goes here
CREATE TABLE steps (
  id SERIAL PRIMARY KEY,
  instruction TEXT NOT NULL,
  recipe_id INTEGER REFERENCES recipes(id),
  duration INTERVAL
);
